package com.steeplesoft.smsbnr

import android.Manifest
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.widget.Button
import android.widget.EditText
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.widget.TextView
import org.json.JSONObject
import android.os.Environment.getExternalStorageDirectory
import org.json.JSONArray
import java.io.File


class MainActivity : AppCompatActivity() {
    val ID_TEXTVIEW = 1
    private val PERMISSION_REQUEST_CODE = 42
    private val perms = listOf(
            Manifest.permission.READ_SMS,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivityUi().setContentView(this)
        checkPermissions()
    }

    fun tryLogin(ui: AnkoContext<MainActivity>, name: CharSequence?, password: CharSequence?) {
        ui.doAsync {
            Thread.sleep(500)

            activityUiThreadWithContext {
                if (checkCredentials(name.toString(), password.toString())) {
                    toast("Logged in! :)")
//                    startActivity<CountriesActivity>()
                } else {
                    toast("Wrong password :( Enter user:password")
                }
            }
        }
    }

    fun backup(ui: AnkoContext<MainActivity>) {
        ui.doAsync {
            val contentResolver = contentResolver
            val projection = arrayOf("*")
            val uri = Uri.parse("content://mms-sms/conversations/")
            val query = contentResolver.query(uri, projection, null, null, "normalized_date desc")
            activityUiThreadWithContext {
                val tv = find<TextView>(ID_TEXTVIEW)
                for (i in 0..query.columnCount - 1) {
                    Log.i("smsbnr", query.columnNames[i])
                    tv.text = "${tv.text}\n${query.columnNames[i]}"
                }
                if (query.moveToFirst()) {
                    val dir = File("${Environment.getExternalStorageDirectory()}/Download/smsbnr/")
                    dir.mkdirs() // creates needed dirs
                    val file = File(dir, "smsbnr.txt")
                    val msgs = JSONArray()
                    do {
                        val msg = JSONObject()
                        for (i in 0..query.columnCount - 1) {
                            msg.put(query.columnNames[i], query.getString(i))
                        }
                        msgs.put(msg)

//                    val string = query.getString(query.getColumnIndex("ct_t"))
//                    if ("application/vnd.wap.multipart.related" == string) {
//
//                    }
                    } while (query.moveToNext())
                    file.writeText(msgs.toString(4))
                    toast("Messages backed up! :)")
                }
            }
        }
    }

    private fun checkCredentials(name: String, password: String) = name == "user" && password == "password"

    private fun checkPermissions(): Boolean {
        val missingPerms = perms.filter {
            ActivityCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED
        } //.contains(false)
        if (!missingPerms.isEmpty()) {
            val array = missingPerms.toTypedArray()
            ActivityCompat.requestPermissions(this, array, PERMISSION_REQUEST_CODE)
        }
        return missingPerms.isEmpty()
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> {
                if (grantResults.size != perms.size || grantResults.contains(PackageManager.PERMISSION_DENIED)) {
                    AlertDialog.Builder(this)
                            .setCancelable(false)
                            .setTitle("Error")
                            .setMessage("This app requires access to text messages and contacts. Click OK to close.")
                            .setPositiveButton("OK", { _, _ -> /*finish()*/ }).create()
                            .show()
                }
            }
        }
    }
}

class MainActivityUi : AnkoComponent<MainActivity> {
    private val customStyle = { v: Any ->
        when (v) {
            is Button -> v.textSize = 26f
            is EditText -> v.textSize = 24f
        }
    }

    override fun createView(ui: AnkoContext<MainActivity>) = with(ui) {
        verticalLayout {
            padding = dip(32)

            imageView(android.R.drawable.ic_menu_save).lparams {
                margin = dip(16)
                gravity = Gravity.CENTER
            }

            button("Backup Messages") {
                onClick {
                    ui.owner.backup(ui)
                }
            }

            scrollView {
                textView {
                    id = ui.owner.ID_TEXTVIEW
                }
            }

//            myRichView()
        }.applyRecursively(customStyle)
    }

}